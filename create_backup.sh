#! /usr/bin/env bash
# /create_backup.sh

cd /home/container

if [[ ! -d /home/container/backups ]]; then
  mkdir -p /home/container/backups
fi

if [[ ! -e /home/container/server.properties ]]; then
  echo "Cannot create backup! No server properties found!"
  exit 0
else
  LEVEL_NAME="$(cat /home/container/server.properties | grep "level-name")"
  LEVEL_NAME="$(echo "${LEVEL_NAME}" | cut -d '=' -f 2)"

  if [[ -z "$LEVEL_NAME" ]]; then
    echo "Failed to read level name!"
    exit 1
  else
    echo "Using level name '${LEVEL_NAME}' for backups."
  fi

  if [[ -d "/home/container/${LEVEL_NAME}" ]]; then
    BACKUP_FILE="/home/container/backups/$(date +"%y-%m-%d_%T").tar.gz"
    echo "Writing backup from '${LEVEL_NAME}' to: '${BACKUP_FILE}'"
    if ! tar -czf "${BACKUP_FILE}" "/home/container/${LEVEL_NAME}"; then
        exit 1
    fi
  else
    echo "Not creating backup. World folder not found: /home/container/${LEVEL_NAME}"
  fi
fi
