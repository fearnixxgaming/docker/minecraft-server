import sys
import json
import os
import requests
import wget
import hashlib
import zipfile
import shutil
import deleteutil

container_home = '/home/container'

if not os.path.isdir('cache'):
    os.makedirs('cache')

def download_file(file_url, md5_checksum):
    file_name = 'cache/' + md5_checksum + '.zip'
    if os.path.exists(file_name):
        print("DEBUG Cached " + file_url)
        return True

    print("DEBUG Downloading " + file_url)
    wget.download(file_url, file_name)
    hash = hashlib.md5()
    with open(file_name, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b''):
            hash.update(chunk)

    print("DEBUG Checking file hash for " + file_name + " is:" + hash.hexdigest() + " should:" + md5_checksum)
    if not (md5_checksum == hash.hexdigest()):
        print("WARN checksum mismatch: " + file_name)
        print("WARN expect: " + md5_checksum + " got:" + hash.hexdigest())
        return False

    return True


def extract_file(file_name, solder_index):
    print("DEBUG Examining " + file_name)
    should_extract = False
    if not (file_name in solder_index):
        solder_index[file_name] = []

    index_entry = solder_index[file_name]
    zip = zipfile.ZipFile(file_name)
    my_zip_infos = zip.infolist()
    for myZipInfo in my_zip_infos:
        the_file_path = myZipInfo.filename
        if not (the_file_path in index_entry):
            index_entry.append(the_file_path)

        if not (os.path.exists(container_home + '/' + the_file_path)):
            should_extract = True
            print("INFO Missing file entry: " + the_file_path)

    if (should_extract):
        print("INFO Extracting " + file_name)
        zip.extractall(container_home)

    zip.close()


if not os.path.exists('settings.json'):
    with open('settings.json', 'w') as file:
        json.dump(json.loads('{"last_build": "", "exclude": [""]}'), file)

with open('settings.json', 'r') as file:
    _config = json.loads(file.read())

if not os.path.exists('index.json'):
    with open('index.json', 'w') as file:
        json.dump(json.loads('{}'), file)

with open('index.json', 'r') as file:
    _index = json.loads(file.read())

solder_url = os.environ.get('SOLDER_URL')
solder_pack = os.environ.get('SOLDER_SLUG')
solder_tag = os.environ.get('SOLDER_TAG')
if solder_tag == "":
    solder_tag = 'recommended'

r = requests.get(solder_url + '/modpack/' + solder_pack)
if r.status_code != 200:
    print("ERR Unexpected status code: " + r.status_code)
    sys.exit(1)

_modpack_info = json.loads(r.text)

if solder_tag == 'recommended' or solder_tag == 'latest':
    solder_tag = _modpack_info[solder_tag]

if solder_tag not in _modpack_info['builds']:
    print("ERR Tag not found: " + solder_pack)
    sys.exit(1)

r = requests.get(solder_url + '/modpack/' + solder_pack + '/' + solder_tag)
if r.status_code != 200:
    print("ERR Unexpected status code: " + r.status_code)
    sys.exit(1)


def delete_filter(file_path):
    slug = file_path.split('-')[0].lower()
    return not ('.plugin' in file_path) and not (slug in _config['exclude'])


_tag_info = json.loads(r.text)
tagDiffers = not (solder_tag == _config['last_build'])
if not tagDiffers:
    print("INFO Tag does not differ. Assuming no update needed")
else:
    shutil.rmtree('cache', ignore_errors=False)
    deleteutil.rmdir_ext(
        file_path='/home/container/mods',
        file_filter=delete_filter
    )
    os.makedirs('cache')

downloadError = False
for mod in _tag_info['mods']:
    print("INFO Found mod " + mod['name'] + " at " + mod['version'])
    zipName = 'cache/' + mod['md5'] + '.zip'
    if mod['name'] in _config['exclude']:
        print("INFO Mod excluded")
        continue

    if not download_file(mod['url'], mod['md5']):
        print("ERR Failed to download mod")
        downloadError = True
        continue

    extract_file(zipName, _index)

if downloadError:
    print("ERR Pack has not been installed correctly")
    sys.exit(1)

_config['last_build'] = solder_tag
with open('settings.json', 'w') as file:
    json.dump(_config, file)

with open('index.json', 'w') as file:
    json.dump(_index, file)
