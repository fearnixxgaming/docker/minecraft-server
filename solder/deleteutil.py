import os
from os import path


def rmdir_ext(file_path, file_filter):
    """Delete all files for which the filter function returns true - recursively"""
    is_dir = path.isdir(file_path)
    if is_dir:
        files = os.listdir(file_path)
    else:
        files = [file_path]

    for file in files:
        file = path.join(file_path, file)
        if path.isdir(file):
            rmdir_ext(
                file_path=file,
                file_filter=file_filter
            )
        elif path.isfile(file) and file_filter(file):
            print("Removing: " + file)
            os.remove(file)
        else:
            print("Skipping: " + file)

    if path.isdir(file_path) and len(os.listdir(file_path)) <= 0:
        os.removedirs(file_path)
