#!/bin/bash

## Function definition ##

function setup_forge() {
	JAR="forge-${FORGE_VERSION}-installer.jar"
	if [ ! -f ${JAR} ]; then
		echo "Downloading Forge installer"
		URL="http://files.minecraftforge.net/maven/net/minecraftforge/forge/${FORGE_VERSION}/forge-${FORGE_VERSION}-installer.jar"
		echo "Downloading: $URL"
		if [ -f ${JAR} ]; then
			rm ${JAR}
		fi
		curl -o ${JAR} -sSL ${URL}
		echo "Running installer"
		java -jar ${JAR} --installServer
	fi
}

function setup_sponge_forge() {
	mkdir -p mods
	JAR="mods/spongeforge-${SPONGE_VERSION}.jar"
	if [ ! -f ${JAR} ]; then
		echo "Downloading SpongeForge"
		URL="https://repo.spongepowered.org/maven/org/spongepowered/spongeforge/${SPONGE_VERSION}/spongeforge-${SPONGE_VERSION}.jar"
		echo "Downloading: $URL"
		curl -o ${JAR} -sSL ${URL}
	fi
}

function setup_sponge_vanilla() {
	#JAR="spongevanilla-${SPONGE_VERSION}.jar"
	if [ ! -f ${SERVER_JARFILE} ]; then
		echo "Downloading SpongeVanilla"
		URL="https://repo.spongepowered.org/maven/org/spongepowered/spongevanilla/${SPONGE_VERSION}/spongevanilla-${SPONGE_VERSION}.jar"
		echo "Downloading: $URL"
		curl -o ${SERVER_JARFILE} -sSL ${URL}
	fi
}

function setup_waterfall() {
	if [[ ! -f "${SERVER_JARFILE}" ]]; then
		echo "Downloading waterfall..."
		curl -o "${SERVER_JARFILE}" "https://papermc.io/ci/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar"
	fi
}

function setup_solder() {
	DIR=$(pwd)
	if [ ! -e solder/solder-sync.py ]; then
		mkdir solder
		curl -o solder/solder-sync.py -sSL https://gitlab.com/fearnixxgaming/docker/minecraft-server/raw/master/solder/solder-sync.py
	fi
	if [ ! -e solder/deleteutil.py ]; then
	    curl -o solder/deleteutil.py -sSL https://gitlab.com/fearnixxgaming/docker/minecraft-server/raw/master/solder/deleteutil.py
	fi

	cd solder
	python solder-sync.py
	if [ ! $! == 0 ]; then
		echo "Python setup script failed!"
		exit 1
	fi
	cd ${DIR}
}

function setup_spigot() {
	DIR=$(pwd)
	if [ ! -e "$SERVER_JARFILE" ]; then
		echo "Setting up BuildTools"
		URL="https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar"
		if [ ! -d buildtools ]; then
			mkdir buildtools
		fi
		cd buildtools
		curl -o BuildTools.jar -sSL $URL
		git config --global --unset core.autocrlf
		JAR_EXTRA=""
		SPIGOT_JAR="spigot.jar"
		if [ ! -z "$SPIGOT_VERSION" ]; then
			JAR_EXTRA="${JAR_LINE} --rev=${SPIGOT_VERSION}"
			SPIGOT_JAR="spigot-${SPIGOT_VERSION}.jar"
		fi
		java -jar BuildTools.jar $JAR_EXTRA
		if [ ! $! == 0 ] || [ ! -e ${SPIGOT_JAR} ] ; then
			echo "Build failed!"
			exit 1
		else
			echo "Linking jar file"
			if [ ! -e ${SERVER_JARFILE} ]; then
				rm ${SERVER_JARFILE}
			fi
			ln -s ${SPIGOT_JAR} ${SERVER_JARFILE}
		fi
	fi
	cd ${DIR}
}

## RUNTIME ##
cd /home/container

if [ "$(pwd)" == "/home/container" ]; then
	git config --global user.email "technik@fearnixx.de"
	git config --global user.name "FearNixx Technik"
fi

# Evaluate update/download/install
if [ ! -z $SERVER_BRANCH ]; then
	echo "Detected SERVER_BRANCH: ${SERVER_BRANCH}"
	BASEPATH="/home/container/"
	case "$SERVER_BRANCH" in
		SPONGEFORGE)
			export SERVER_JARFILE="${BASEPATH}forge-${FORGE_VERSION}-universal.jar"
			setup_sponge_forge
			setup_forge
		;;
		SPONGEVANILLA)
			export SERVER_JARFILE="${BASEPATH}spongevanilla-${SPONGE_VERSION}.jar"
			setup_sponge_vanilla
		;;
		FORGE)
			export SERVER_JARFILE="${BASEPATH}forge-${FORGE_VERSION}-installer.jar"
			setup_forge
		;;
		WATERFALL)
			export SERVER_JARFILE="${BASEPATH}Waterfall.jar"
			setup_waterfall
		;;
		SOLDER)
			export SERVER_JARFILE="${BASEPATH}forge-${FORGE_VERSION}-universal.jar"
			setup_solder
			setup_forge
			setup_sponge_forge
		;;
		SPIGOT)
			export SERVER_JARFILE="${BASEPATH}spigot.jar"
			setup_spigot
		;;
		*)
			echo "Unknown server branch: ${SERVER_BRANCH}"
			exit 1
	esac
else
	echo "Error: SERVER_BRANCH not set!"
	exit 1
fi

# Output Current Java Version
java -version

# Make internal Docker IP address available to processes.
export INTERNAL_IP=`ip route get 1 | awk '{print $NF;exit}'`
echo "IP today: $INTERNAL_IP"

# Before start, create backups!
if [[ -e /create_backup.sh ]] && [[ -z "$SKIP_BACKUPS" ]]; then
	if ! /create_backup.sh; then
		echo "Failed to create backup. REFUSING TO START!"
		exit 1
	fi
else
	echo "Backups skipped. Not found or disabled."
fi

# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`
echo ":/home/container$ ${MODIFIED_STARTUP}"

# Run the Server
eval ${MODIFIED_STARTUP}

# Example startup line: java -Xmx{{SERVER_XMX}}M -Xms{{SERVER_XMS}}M {{EXTRA_JVM}} -jar {{SERVER_JARFILE}} {{EXTRA_APP}}
