# Dockerfile taken from Pterodactyl/Containers/java-glibc
FROM openjdk:8-jdk-buster

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

RUN apt update \
    && apt upgrade -y \
    && apt install -y curl ca-certificates openssl git maven tar bash sqlite fontconfig \
	&& apt install -y python3 python3-pip \
	&& pip3 install wget requests \
    && useradd -d /home/container container

USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh
COPY ./create_backup.sh /create_backup.sh

CMD ["/bin/bash", "/entrypoint.sh"]
